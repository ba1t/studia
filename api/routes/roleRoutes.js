'use strict';
const role = require('../controllers/roleController');

module.exports = (app) => {     
    app.route('/api/role')
        .get(role.list)
        .post(role.add)
    app.route('/api/role/:id')
        .get(role.get)
        .put(role.update) 
        .delete(role.delete)  
}
