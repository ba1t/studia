'use strict';
const productStatus = require('../controllers/statusController');

module.exports = (app) => {
    app.route('/api/status')
        .post(productStatus.add)
        .get(productStatus.list)        
    app.route('/api/status/:id')
        .get(productStatus.get)
        .put(productStatus.update)
        .delete(productStatus.delete);
}
