'use strict';
const customer = require('../controllers/customerController');

module.exports = (app, passport) => {
    app.route('/api/customer')
        .get(customer.list)
        .post(customer.add)
    app.route('/api/customer/:id')
        .get(customer.get)
        .put(customer.update)
        .delete(customer.delete)
    app.route('/api/customer/:id/review')
        .get(customer.getReview)
    app.route('/api/customer/:id/basket')
        .get(customer.getBasket)
    app.route('/api/customer/:id/order')
        .get(customer.getOrder)
}
