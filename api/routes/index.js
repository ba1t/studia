
module.exports = (app, passport) => {
    require('./productRoutes')(app, passport);
    require('./categoryRoutes')(app, passport);
    require('./manufacturerRoutes')(app, passport);
    require('./statusRoutes')(app, passport);
    require('./reviewRoutes')(app, passport);
    require('./taxRoutes')(app, passport);
    require('./basketRoutes')(app, passport);
}