'use strict';
const basket = require('../controllers/basketController');

module.exports = (app) => {     
    app.route('/api/basket')
        .get(basket.list)
        .post(basket.add)
    app.route('/api/basket/:id')
        .get(basket.get)
        .put(basket.update) 
        .delete(basket.delete)  
}
