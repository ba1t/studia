'use strict';
const user = require('../controllers/userController');

module.exports = (app) => {     
    app.route('/api/user')
        .get(user.list)
        .post(user.add)
    app.route('/api/user/:id')
        .get(user.get)
        .put(user.update) 
        .delete(user.delete)  
}
