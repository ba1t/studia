'use strict';
const delivery = require('../controllers/deliveryController');

module.exports = (app) => {     
    app.route('/api/delivery')
        .get(delivery.list)
        .post(delivery.add);
    app.route('/api/delivery/:id')
        .get(delivery.get)
        .put(delivery.update) 
        .delete(delivery.delete)  
}
