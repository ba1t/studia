'use strict';
const tax = require('../controllers/taxController');

module.exports = (app) => {
    app.route('/api/tax')
        .get(tax.list)
        .post(tax.add)
    app.route('/api/tax/:id')
        .get(tax.get)
        .put(tax.update)
        .delete(tax.delete);
}
