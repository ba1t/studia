'use strict';
const product = require('../controllers/productController');

module.exports = (app, passport) => {
    app.route('/api/product')
        .get(product.list)
        .post(product.add)
    app.route('/api/product/:id')
        .get(product.get)
        .put(product.update)
        .delete(product.delete)
    app.route('/api/product/:id/review')
        .get(product.getReview)
}
