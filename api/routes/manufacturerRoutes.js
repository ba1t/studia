'use strict';
const manufacturer = require('../controllers/manufacturerController');

module.exports = (app) => {
    app.route('/api/manufacturer')
        .get(manufacturer.list)
        .post(manufacturer.add)
    app.route('/api/manufacturer/:id')
        .get(manufacturer.get)
        .put(manufacturer.update)
        .delete(manufacturer.delete);
}
