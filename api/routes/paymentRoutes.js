'use strict';
const payment = require('../controllers/paymentController');

module.exports = (app) => {     
    app.route('/api/payment')
        .get(payment.list)
        .post(payment.add)
    app.route('/api/payment/:id')
        .get(payment.get)
        .put(payment.update) 
        .delete(payment.delete)  
}
