'use strict';
const category = require('../controllers/categoryController');

module.exports = (app) => {     
    app.route('/api/category')
        .get(category.list)
        .post(category.add)
    app.route('/api/category/:id')
        .get(category.get)
        .put(category.update) 
        .delete(category.delete)  
}
