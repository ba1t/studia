'use strict';
const reviewStatus = require('../controllers/reviewController');

module.exports = (app) => {
    app.route('/api/review')
        .post(reviewStatus.add)
        .get(reviewStatus.list)        
    app.route('/api/review/:id')
        .get(reviewStatus.get)
        .put(reviewStatus.update)
        .delete(reviewStatus.delete);
}
