'use strict';
const order = require('../controllers/orderController');

module.exports = (app) => {     
    app.route('/api/order')
        .get(order.list)
        .post(order.add);
    app.route('/api/order/:id')
        .get(order.get)
        .put(order.update) 
        .delete(order.delete)  
}
