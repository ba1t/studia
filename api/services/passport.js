const passport = require('passport');
const BearerStrategy = require('passport-http-bearer');
const mongoose = require('mongoose');
const User = mongoose.model('User');

passport.use(new BearerStrategy(
  function(token, done) {
    User.findOne({ token: token }, (err, user) => {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });
  }
));