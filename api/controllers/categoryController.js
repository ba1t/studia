'use strict';
const mongoose = require('mongoose');
const Category = mongoose.model('Category');

module.exports = {
    list: (req, res) => {
        Category.find({}, (err, category) => {
            try {
                res.json({success: true, data: category});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Category.findById(req.params.id, (err, category) => {
            try {
                res.json({success: true, data: category});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newCategory = new Category(req.body);       
        newCategory.save((err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Category.findByIdAndUpdate(req.params.id, req.body, (err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Category.findByIdAndRemove(req.params.id, (err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
