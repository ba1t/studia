'use strict';
const mongoose = require('mongoose');
const User = mongoose.model('User');

module.exports = {
    list: (req, res) => {
        User.find({}, (err, user) => {
            try {
                res.json({success: true, data: user});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        User.findById(req.params.id, (err, user) => {
            try {
                res.json({success: true, data: user});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newUser = new User(req.body);       
        newUser.save((err, user) => {
            try {
                res.json({success: true, data: user});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        User.findByIdAndUpdate(req.params.id, req.body, (err, user) => {
            try {
                res.json({success: true, data: user});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        User.findByIdAndRemove(req.params.id, (err, user) => {
            try {
                res.json({success: true, data: user});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
