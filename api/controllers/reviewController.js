'use strict';
const mongoose = require('mongoose');
const Review = mongoose.model('Review');

module.exports = {
    list: (req, res) => {
        Review.find({}, (err, review) => {
            try {
                res.json({success: true, data: review});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Review.findById(req.params.id, (err, review) => {
            try {
                res.json({success: true, data: review});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newReview = new Review(req.body);       
        newReview.save((err, review) => {
            try {
                res.json({success: true, data: review});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Review.findByIdAndUpdate(req.params.id, req.body, (err, review) => {
            try {
                res.json({success: true, data: review});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Review.findByIdAndRemove(req.params.id, (err, review) => {
            try {
                res.json({success: true, data: review});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
