'use strict';
const mongoose = require('mongoose');
const Status = mongoose.model('Status');

module.exports = {
    list: (req, res) => {
        Status.find({ type: req.params.type }, (err, category) => {
            try {
                res.json({success: true, data: category});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Status.findById(req.params.id, (err, category) => {
            try {
                res.json({success: true, data: category});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newStatus = new Status(req.body);       
        newStatus.save((err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Status.findByIdAndUpdate(req.params.id, req.body, (err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Status.findByIdAndRemove(req.params.id, (err, category) => {
            try {
                res.json({success: true, data: category});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
