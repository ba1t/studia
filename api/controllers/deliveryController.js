'use strict';
const mongoose = require('mongoose');
const Delivery = mongoose.model('Delivery');

module.exports = {
    list: (req, res) => {
        Delivery.find({}, (err, delivery) => {
            try {
                res.json({success: true, data: delivery});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Delivery.findById(req.params.id, (err, delivery) => {
            try {
                res.json({success: true, data: delivery});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newDelivery = new Delivery(req.body);       
        newDelivery.save((err, delivery) => {
            try {
                res.json({success: true, data: delivery});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Delivery.findByIdAndUpdate(req.params.id, req.body, (err, delivery) => {
            try {
                res.json({success: true, data: delivery});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Delivery.findByIdAndRemove(req.params.id, (err, delivery) => {
            try {
                res.json({success: true, data: delivery});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
