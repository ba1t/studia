'use strict';
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Review = mongoose.model('Review');
const Order = mongoose.model('Order');
const Basket = mongoose.model('Basket');
module.exports = {
    list: (req, res) => {
        Product.find({}).populate('tax').exec((err, product) => {
            try {
                res.json({success: true, data: product});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Product.findById(req.params.id, (err, product) => {
            try {
                res.json({success: true, data: product});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newProduct = new Product(req.body);       
        newProduct.save((err, product) => {
            try {
                res.json({success: true, data: product});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Product.findByIdAndUpdate(req.params.id, req.body, (err, product) => {
            try {
                res.json({success: true, data: product});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Product.findByIdAndRemove(req.params.id, (err, product) => {
            try {
                res.json({success: true, data: product});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    getReview: (req, res)  => {
        Review.find( { _id: ObjectId(req.params.id) }, (err, review) => {
            try {
                res.json({success: true, data: review});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    }
};
