'use strict';
const mongoose = require('mongoose');
const Order = mongoose.model('Order');

module.exports = {
    list: (req, res) => {
        Order.find({}, (err, order) => {
            try {
                res.json({success: true, data: order});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Order.findById(req.params.id, (err, order) => {
            try {
                res.json({success: true, data: order});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newOrder = new Order(req.body);       
        newOrder.save((err, order) => {
            try {
                res.json({success: true, data: order});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Order.findByIdAndUpdate(req.params.id, req.body, (err, order) => {
            try {
                res.json({success: true, data: order});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Order.findByIdAndRemove(req.params.id, (err, order) => {
            try {
                res.json({success: true, data: order});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
