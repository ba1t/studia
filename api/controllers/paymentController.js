'use strict';
const mongoose = require('mongoose');
const Payment = mongoose.model('Payment');

module.exports = {
    list: (req, res) => {
        Payment.find({}, (err, payment) => {
            try {
                res.json({success: true, data: payment});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Payment.findById(req.params.id, (err, payment) => {
            try {
                res.json({success: true, data: payment});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newPayment = new Payment(req.body);       
        newPayment.save((err, payment) => {
            try {
                res.json({success: true, data: payment});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Payment.findByIdAndUpdate(req.params.id, req.body, (err, payment) => {
            try {
                res.json({success: true, data: payment});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Payment.findByIdAndRemove(req.params.id, (err, payment) => {
            try {
                res.json({success: true, data: payment});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
