'use strict';
const mongoose = require('mongoose');
const Tax = mongoose.model('Tax');

module.exports = {
    list: (req, res) => {
        Tax.find({}, (err, tax) => {
            try {
                res.json({success: true, data: tax});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Tax.findById(req.params.id, (err, tax) => {
            try {
                res.json({success: true, data: tax});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newTax = new Tax(req.body);       
        newTax.save((err, tax) => {
            try {
                res.json({success: true, data: tax});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Tax.findByIdAndUpdate(req.params.id, req.body, (err, tax) => {
            try {
                res.json({success: true, data: tax});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Tax.findByIdAndRemove(req.params.id, (err, tax) => {
            try {
                res.json({success: true, data: tax});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
