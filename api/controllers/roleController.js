'use strict';
const mongoose = require('mongoose');
const Role = mongoose.model('Role');

module.exports = {
    list: (req, res) => {
        Role.find({}, (err, role) => {
            try {
                res.json({success: true, data: role});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Role.findById(req.params.id, (err, role) => {
            try {
                res.json({success: true, data: role});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newRole = new Role(req.body);       
        newRole.save((err, role) => {
            try {
                res.json({success: true, data: role});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Role.findByIdAndUpdate(req.params.id, req.body, (err, role) => {
            try {
                res.json({success: true, data: role});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Role.findByIdAndRemove(req.params.id, (err, role) => {
            try {
                res.json({success: true, data: role});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
