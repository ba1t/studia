'use strict';
const mongoose = require('mongoose');
const Manufacturer = mongoose.model('Manufacturer');

module.exports = {
    list: (req, res) => {
        Manufacturer.find({}, (err, manufacturer) => {
            try {
                res.json({success: true, data: manufacturer});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Manufacturer.findById(req.params.id, (err, manufacturer) => {
            try {
                res.json({success: true, data: manufacturer});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newManufacturer = new manufacturer(req.body);       
        newManufacturer.save((err, manufacturer) => {
            try {
                res.json({success: true, data: manufacturer});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Manufacturer.findByIdAndUpdate(req.params.id, req.body, (err, manufacturer) => {
            try {
                res.json({success: true, data: manufacturer});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Manufacturer.findByIdAndRemove(req.params.id, (err, manufacturer) => {
            try {
                res.json({success: true, data: manufacturer});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
