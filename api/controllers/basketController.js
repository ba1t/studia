'use strict';
const mongoose = require('mongoose');
const Basket = mongoose.model('Basket');

module.exports = {
    list: (req, res) => {
        Basket.find({})
            .populate('Status')
            .populate('Product')
            .populate('Customer')
            .exec((err, basket) => {
                try {
                    res.json({success: true, data: basket});                
                } catch (err) {
                    res.json({success: false, message: err})
                }
            })
    },
    get: (req, res) => {
        Basket.findById(req.params.id)
            .populate('Status')
            .populate('Product')
            .populate('Customer')
            .exec((err, basket) => {
                try {
                    res.json({success: true, data: basket});                
                } catch (err) {
                    res.json({success: false, message: err})
                }
            });
    },
    add: (req, res) => {
        let newBasket = new Basket(req.body);       
        newBasket.save((err, basket) => {
            try {
                res.json({success: true, data: basket});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Basket.findByIdAndUpdate(req.params.id, req.body, (err, basket) => {
            try {
                res.json({success: true, data: basket});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Basket.findByIdAndRemove(req.params.id, (err, basket) => {
            try {
                res.json({success: true, data: basket});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    }
};
