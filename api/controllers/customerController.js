'use strict';
const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');
const Review = mongoose.model('Review');
const Basket = mongoose.model('Basket');
const Order = mongoose.model('Order');

module.exports = {
    list: (req, res) => {
        Customer.find({})
            .populate('Status')
            .limit(req.query.limi || 20)
            .exec((err, customer) => {
            try {
                res.json({success: true, data: customer});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    get: (req, res) => {
        Customer.findById(req.params.id)
            .populate('Status')
            .exec((err, customer) => {
            try {
                res.json({success: true, data: customer});                
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    add: (req, res) => {
        let newCustomer = new Customer(req.body);       
        newCustomer.save((err, customer) => {
            try {
                res.json({success: true, data: customer});                                           
            } catch (err) {
                res.json({status: false, message: err})
            }     
        });
    },
    update: (req, res) => {
        Customer.findByIdAndUpdate(req.params.id, req.body, (err, customer) => {
            try {
                res.json({success: true, data: customer});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    delete: (req, res) => {
        Customer.findByIdAndRemove(req.params.id, (err, customer) => {
            try {
                res.json({success: true, data: customer});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        });
    },
    getReview: (req, res)  => {
        Review.find( { _id: ObjectId(req.params.id) }, (err, review) => {
            try {
                res.json({success: true, data: review});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    getBasket: (req, res)  => {
        Basket.find( { _id: ObjectId(req.params.id) }, (err, basket) => {
            try {
                res.json({success: true, data: basket});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    },
    getOrder: (req, res)  => {
        Order.find( { _id: ObjectId(req.params.id) }, (err, order) => {
            try {
                res.json({success: true, data: order});                                           
            } catch (err) {
                res.json({success: false, message: err})
            }
        })
    }
};
