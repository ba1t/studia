'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const basketSchema = new Schema ({
    customer: { type: Schema.ObjectId, ref: 'Customer' },
    status: { type: Schema.ObjectId, ref: 'Status' },
    product: [
        {
            productId: { type: Schema.ObjectId, ref: 'Product' },
            quantity: { type: Number, default: 1 }
        }
    ]
});

module.exports = mongoose.model('Basket', basketSchema);