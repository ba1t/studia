'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const taxSchema = new Schema ({
    title: { type: String, require: "pole wymagane" },
    description: String,
    taxRate: {type: Number, required: "Podaj wartość podatku"}
})

module.exports = mongoose.model('Tax', taxSchema);
