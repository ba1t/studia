'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const manufacturerSchema = new Schema ({
    name : { type: String, required: "Wymagana nazwa producenta"},
    logo: {
        url : String, 
        attributes : {
            alt : String, 
            title : String
        }
    }
});

module.exports = mongoose.model('Manufacturer', manufacturerSchema);