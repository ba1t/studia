'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const paymentSchema = new Schema ({
    name: { type: String, require: true },
    accountNumber: { type: Number, required: true }
});

module.exports = mongoose.model('Payment', paymentSchema);