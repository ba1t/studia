'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const customerSchema = new Schema ({
    firstName: { type: String, require: true },
    lastName: { type: String, require: true },
    email: { type: String, require: true, validate: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    passwd: { type: String},
    newsletter: Boolean,
    phone: { type: String },
    birtchDate: Date,
    created: { type: Date, default: Date.now },
    update: [{
        date: { type: Date, default: Date.now },
        change: { type: Object }
    }],
    status: { type: Schema.ObjectId, ref: 'Status' }, 
    adress: [{
        street: { type: String },
        house: { type: String },
        flat: { type: String },
        city: { type: String },
        zipCode: { type: String, validate: /[0-9]{2}\-[0-9]{3}/, default: "00-000" },
    }],
    billing: [{
        company: { type: Boolean, require: false },
        fistName: { type: String },
        lastName: { type: String },
        companyName: { type: String },
        nip: { type: Number, validate: /[0-9]{10}/ },
        adress: {
            street: { type: String },
            house: { type: String },
            flat: { type: String },
            city: { type: String },
            zipCode: { type: String },
        }
    }]
})

module.exports = mongoose.model('Customer', customerSchema);
