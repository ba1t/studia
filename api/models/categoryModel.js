'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const categorySchema = new Schema ({
    name : { type: String, required: "Wymagana nazwa kategorii"},
    active: { type: Boolean, require: true },
    image: {
        url : String, 
        attributes : {
            alt : String, 
            title : String
        }
    }, 
    weight : Number, 
    parentCategory : { type: Schema.ObjectId, ref: 'Category' },

});

module.exports = mongoose.model('Category', categorySchema);