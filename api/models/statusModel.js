'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const statusSchema = new Schema ({
    type: { type: String },
    status : { type: String, required: "Wymagana nazwa statusu"},
});

module.exports = mongoose.model('Status', statusSchema);