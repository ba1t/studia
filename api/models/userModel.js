'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema ({
    email: { type: String, require: true, validate: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    passwd: { type: String, require: true },
    firstName: { type: String },
    lastName: { type: String },
    roles: [{ type: Schema.ObjectId, ref: 'Role'}]
})

module.exports = mongoose.model('User', userSchema);
