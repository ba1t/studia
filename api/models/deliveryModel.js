'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const deliverySchema = new Schema ({
    name: {type: String, require: true},
    options: [{
        weight: { type: Number, require: true },
        height: { type: Number, default: 0},
        width: { type: Number, default: 0 },
        depth: { type: Number, default: 0 },
        price: { type: Number, require: true }
    }]
});

module.exports = mongoose.model('Delivery', deliverySchema);