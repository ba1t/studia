const productModel = require('./productModel');
const productStatusModel = require ('./statusModel');
const categoryModel = require ('./categoryModel');
const reviewModel = require ('./reviewModel');
const manufacturerModel = require ('./manufacturerModel');
const taxModel = require ('./taxModel');
const userModel = require ('./userModel');
const customerModel = require ('./customerModel');
const basketModel = require ('./basketModel');
const orderModel = require ('./orderModel');
const deliveryModel = require ('./deliveryModel');

module.exports = {
    productModel,
    categoryModel,
    manufacturerModel,
    taxModel,
    productStatusModel,
    reviewModel,    
    userModel,
    customerModel,
    orderModel,
    deliveryModel
}