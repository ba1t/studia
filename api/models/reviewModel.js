'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const reviewSchema = new Schema ({
    rating: { type: Number, min: 1, max: 5 },
    text: String,
    reviewer : { type: Schema.ObjectId, ref: 'Customer' },
    product : { type: Schema.ObjectId, ref: 'Product' }
})

module.exports = mongoose.model('Review', reviewSchema);
