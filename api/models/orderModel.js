'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const orderSchema = new Schema ({
    customer: {
        customerId: { type: Schema.ObjectId, ref: 'Customer' },
        fistName: { type: String, require: true },
        lastName: { type: String, require: true },
        email: { type: String, require: true, validate: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    },
    status: { type: Schema.ObjectId, ref: 'Status' },
    products: [
        {
            productId: { type: Schema.ObjectId, ref: 'Product' },
            price: { type: Number, require: true },
            tax: { type: String, require: true },
            quantity: { type: Number, default: 1 }
        }
    ],
    dalivery: {
        deliveryId: { type: Schema.ObjectId, ref: 'Delivery'},
        name: { type: String, require: true },
        price: { type: Number, require: true },
        adress: {
            street: { type: String },
            house: { type: String },
            flat: { type: String },
            city: { type: String },
            zipCode: { type: String }
        }
    }, 
    payment: {
        paymentId: { type: Schema.ObjectId, ref: 'Payment'},
        name: { type: String, require: true },
        price: { type: Number, require: true }
    },
    fv: { type: Boolean, default: false},
    billing: {
        company: { type: Boolean },
        fistName: { type: String },
        lastName: { type: String },
        companyName: { type: String },
        nip: { type: Number },
        adress: {
            street: { type: String },
            house: { type: String },
            flat: { type: String },
            city: { type: String },
            zipCode: { type: String },
        }
    }
});

module.exports = mongoose.model('Order', orderSchema);