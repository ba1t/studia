'use strict';
const mongoose = require('mongoose');

const { Schema } = mongoose;

const productSchema = new Schema ({
    name: { type: String, require: true },
    body: String,
    published: { type: Boolean, default: false },
    categoriesIds: [ { type: Schema.ObjectId, ref: 'Category' } ],
    manufacturerId: { type: Schema.ObjectId, ref: 'Manufacturer' },
    taxId: { type: Schema.ObjectId, ref: 'Tax' },
    statusId: { type: Schema.ObjectId, ref: 'Status' },
    created: { type: Date, default: Date.now },
    update: { type: Date, default: Date.now },
    available: Date,
    inStock: { type: Number, default: 0 },    
    variants: [{
        type: { type: String, enum: ['color', 'list']},
        name: { type: String },
        options: [{
            active: { type: Boolean },
            properties: { type: Object },
            name: { type: String },
            price: { type: Number, default: 0 },
            inStock: { type: Number, default: 0 },
            imageId: { type: Schema.ObjectId }
        }]
    }],
    prices: [ {
        price: { type: Number } ,
        quantity: Number,
        promotion: {
            active: { type: Boolean, default: false },
            newPrice: String,
            startDate: Date,
            expiryDate: Date
        }
    } ],
    images: [ {
        url: String,
        weight: { type: Number, default: 0},
        attributes: {
            alt: { type: String  },
            title: { type: String }
        }
    } ],
    relatedIds: [
        { type: Schema.ObjectId, ref: 'Product' }
    ],
    details: {
        weight: { type: Number },
        height: { type: Number },
        width: { type: Number },
        depth: { type: Number}
    },
    path: String,   
    metatags: {
        title: { type: String },
        description: { typ: String },
        keywords: String
    }
})

module.exports = mongoose.model('Product', productSchema);
