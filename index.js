const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const confg = require('./api/config.js');

require('./api/models');
require('./api/services/passport');

mongoose.Promise = global.Promise;
mongoose.connect(confg.mongoURL, {useMongoClient: true});

const app = express();
const PORT = process.env.PORT || 5000;

app.use("/media", express.static(__dirname + '/media'))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize());

app.get("/", (req, res) => {
    res.send("api");
})

app.use(require('morgan')('combined'));
require('./api/routes')(app,passport);

app.listen(PORT);

console.log('RESTful API server started on: ' + PORT + " " + __dirname);